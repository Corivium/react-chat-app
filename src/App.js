import React from 'react';
import Chatkit from '@pusher/chatkit-client';
import MessageList from './components/MessageList';
import SendMessageForm from './components/SendMessageForm';
import RoomList from './components/RoomList';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
// import NewRoomForm from './components/NewRoomForm';
import { tokenUrl, instanceLocator, userID } from './config';


export default class App extends React.Component {

    constructor() {
        super();
        this.state = {
            roomId: null,
            messages: [],
            joinableRooms: [],
            joinedRooms: []
            
        };
        this.sendMessage = this.sendMessage.bind(this);
        this.subscribeToRoom = this.subscribeToRoom.bind(this);
        this.getRooms = this.getRooms.bind(this);
    }

    componentDidMount() { // triggered directly after render method is provoked

        const tokenProvider = new Chatkit.TokenProvider({
            url: tokenUrl
        });

        const chatManager = new Chatkit.ChatManager({
            instanceLocator: instanceLocator,
            userId: userID,
            tokenProvider: tokenProvider
        });


        chatManager
        .connect() // make promise
            .then(currentUser => { // get access to currentUser
                //console.log("Connected as user ", currentUser);
                this.currentUser = currentUser;
                this.getRooms();
                
            })
            .catch(err => console.log('error:', err));
    }

    getRooms() {
        // Get room lists
        this.currentUser.getJoinableRooms()
        .then(joinableRooms => {
            this.setState({
                joinableRooms,
                joinedRooms: this.currentUser.rooms
            });
        })
        .catch(err => console.log('error on joinableRooms: ', err));
    }

    subscribeToRoom(roomId) {
        this.setState ({
            messages: []
        });
        // Get Current User Data
        this.currentUser.subscribeToRoomMultipart({
            roomId: roomId,
            //messageLimit: 10,
            hooks: {
                onMessage: message => {
                    // console.log(currentUser.id, message.parts[0].payload.content, message.createdAt);
                    this.setState({
                        messages: [...this.state.messages, message]
                    });
                }
            }
        })
        .then(room => {
            this.setState({
                roomId: room.id
            });
            this.getRooms();
        })
        .catch(err => console.log('error on subscribing to room', err));
    }

    sendMessage(text) {
        this.currentUser.sendMessage({
            text,
            roomId: this.state.roomId
        });
    }

    

    render() {
        return (
        <div className="app container-fluid">
            <div class="row">
                <RoomList 
                    roomID={this.state.roomId}
                    subscribeToRoom={this.subscribeToRoom} 
                    rooms={[...this.state.joinableRooms, ...this.state.joinedRooms]} 
                />
                <MessageList messages={this.state.messages} />
                <SendMessageForm sendMessage={this.sendMessage}/>
                {/*<NewRoomForm /> */}
            </div>
        </div>
        );
    };

}

