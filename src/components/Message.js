import React from 'react';

const Message = ({username, message}) => {

    return (
        <div className="message">
            <div className="message-username">{username}:</div>
            <div className="message-text">{message}</div>
        </div>  
    )

}

export default Message;