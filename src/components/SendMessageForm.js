import React from 'react'

class SendMessageForm extends React.Component {

    constructor() {
        super();
        this.state = {
            message: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    handleChange(e) {
        this.setState({
            message: e.target.value
        });
    };

    handleSubmit(e) {
        e.preventDefault();
        this.props.sendMessage(this.state.message); // grab the submit handler from parent and apply the message from stay and pass back up to parent
        this.setState({
            message: '' // Reset input field to blank
        })
    };

    render() {
        //console.log(this.state.message);
        return (
            <form 
                onSubmit={this.handleSubmit}
                className="send-message-form col-md-10 offset-md-2"
                >
                <input
                    onChange={this.handleChange}
                    value={this.state.message}
                    placeholder="type your message and hit ENTER"
                    type="text" />
            </form>
        )
    }
}

export default SendMessageForm