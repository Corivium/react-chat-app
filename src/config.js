
const tokenUrl = "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/e17c7509-bc88-4ed5-af03-547d1654620b/token";
const instanceLocator = "v1:us1:e17c7509-bc88-4ed5-af03-547d1654620b";
const userID = "Corivium";
//const roomID = "9c2d9f2d-5f62-4c05-ac75-8469c317b963";

export { tokenUrl, instanceLocator, userID};